package com.moveinsync.tools.googleSpeedProfile;

import java.util.Date;

import org.joda.time.DateTime;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.TrafficModel;
import com.google.maps.model.TravelMode;

public enum GoogleAPIService {

	INSTANCE;

	GeoApiContext geoAPIContext = null;

	public DistanceMatrix getDistanceUsingMatrixAPI(String[] geoCord, String[] geoCord1, Date date) throws Exception {

		DateTime departureTime = new DateTime(date);
		
		DistanceMatrixApiRequest req = DistanceMatrixApi
				.newRequest(getGeoApiContext()).mode(TravelMode.DRIVING)
				.origins(geoCord).destinations(geoCord1).trafficModel(TrafficModel.BEST_GUESS)
				.departureTime(departureTime);
		return req.await();

	}

	public GeoApiContext getGeoApiContext() {
		
		String mapClientId = "gme-moveinsynctechnology1";
		String mapSecretKey = "sSNaRMHbQ8i2melqx8tOEPNFTOM=";
		if (geoAPIContext == null) {
			try {
				geoAPIContext = new GeoApiContext().setEnterpriseCredentials(
						mapClientId,
						mapSecretKey);
				geoAPIContext.setQueryRateLimit(60);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		return geoAPIContext;
	}
}
package com.moveinsync.tools.googleSpeedProfile;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class App {
	public static void main(String[] args) throws IOException {
		loadProperties();
		SpeedProfileGenerator.generate();
	}

	public static void loadProperties() {

		try {
			InputStream input = new BufferedInputStream(new FileInputStream(
					"/home/kethan/Desktop/config.properties"));
			Properties properties = new Properties();
			properties.load(input);

			ConfigVariables.initConfigs(properties);
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Issue while reading config file");
		}
	}
}

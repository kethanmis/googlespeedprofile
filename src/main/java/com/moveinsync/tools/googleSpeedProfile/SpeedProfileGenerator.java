package com.moveinsync.tools.googleSpeedProfile;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.maps.model.DistanceMatrix;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Hello world!
 *
 */
public class SpeedProfileGenerator {

	private static FileWriter fileWriter;

	public static final String timeProfileFileName = "timeprofile.csv";
	public static final String pickupFileName = "Pickup_Time_Chart.csv";

	public static void generate() {
		try {
			createTimeProfileFile();
		} catch (IOException e) {
			System.out.println("cannot create timeprofile.csv");
			e.printStackTrace();
		}
		try {
			List<String[]> lines = getLinesFromCSV(ConfigVariables.localityfile, ',');
			List<Locality> localities = getLocalities(lines);
			calculateSpeeds(localities);
			recalculateSpeeds(localities);
			createPickUpTimeChart(localities);
		} catch (IOException e) {
			System.out.println("error reading locality ids");
			e.printStackTrace();
		}
	}

	public static String getTimeOfDay(int hours) {
		return String.format("%02d", hours) + ":00";
	}

	public static String getTimeProfile(int day, int hours) {
		String result = "";
		if (day == 1) {
			result = "Sun";
		} else if (day == 2) {
			result = "Mon";
		} else if (day == 3) {
			result = "Tue";
		} else if (day == 4) {
			result = "Wed";
		} else if (day == 5) {
			result = "Thu";
		} else if (day == 6) {
			result = "Fri";
		} else if (day == 7) {
			result = "Sat";
		}
		result += "-" + String.valueOf(hours);
		return result;
	}
	
	public static List<Locality> getLocalities(List<String[]> lines) {
		List<Locality> localities = new ArrayList<Locality>();
		for (String[] line : lines) {
			Locality tempLocality = new Locality(line[0], line[1], line[2], line[3]);
			localities.add(tempLocality);
		}
		return localities;
	}

	public static List<String[]> getLinesFromCSV(String fileName, char seperator) throws IOException {
		List<String[]> lines = new ArrayList<String[]>();
		CSVReader reader = null;
		reader = new CSVReader(new FileReader(fileName), seperator);
		String[] line;
		line = reader.readNext();
		while ((line = reader.readNext()) != null) {
			lines.add(line);
		}
		reader.close();
		return lines;
	}
	
	private static void recalculateSpeeds(List<Locality> localities) {
		for (int day = 1; day <= 7; ++day) {
			for (int hour = 0; hour < 24; ++hour) {
				List<Locality> tempLocalities = new ArrayList<Locality>();
				for (Locality locality : localities) {
					if (locality.speed[day-1][hour] == -1) {
						tempLocalities.add(locality);
					}
				}
				calculateSpeeds(tempLocalities, day, hour);
			}
		}
	}
	
	private static void calculateSpeeds(List<Locality> localities) {
		int n = localities.size();
		for (int i = 0; i < n; i+=25) {
			int j = Math.min(n, i+25);
			List<String> srcGeo = new ArrayList<String>();
			for (int k = i; k < j; ++k) {
				srcGeo.add(localities.get(k).geocode);
			}
			String[] geoCord = srcGeo.toArray(new String[srcGeo.size()]);
			String[] geoCord1 = new String[] {ConfigVariables.anchorGeocode};
			for (int day = 1; day <= 7; ++day) {
				for (int hour = 0; hour < 24; ++hour) {
					Date date = getDate(day, hour);
					try {
						DistanceMatrix dMatix = GoogleAPIService
								.INSTANCE.getDistanceUsingMatrixAPI(geoCord, geoCord1, date);
						for (int k = i; k < j; ++k) {
							long dist = dMatix.rows[k%25].elements[0].distance.inMeters;
							long time = dMatix.rows[k%25].elements[0].durationInTraffic.inSeconds;
							localities.get(k).distance = ((float)dist)/1000f;
							localities.get(k).speed[day-1][hour] = getSpeedInkmph(dist, time);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	private static void calculateSpeeds(List<Locality> localities, int day, int hour) {
		int n = localities.size();
		for (int i = 0; i < n; i+=25) {
			int j = Math.min(n, i+25);
			List<String> srcGeo = new ArrayList<String>();
			for (int k = i; k < j; ++k) {
				srcGeo.add(localities.get(k).geocode);
			}
			String[] geoCord = srcGeo.toArray(new String[srcGeo.size()]);
			String[] geoCord1 = new String[] {ConfigVariables.anchorGeocode};
			Date date = getDate(day, hour);
			try {
				DistanceMatrix dMatix = GoogleAPIService.INSTANCE.getDistanceUsingMatrixAPI(geoCord, geoCord1, date);
				for (int k = i; k < j; ++k) {
					long dist = dMatix.rows[k % 25].elements[0].distance.inMeters;
					long time = dMatix.rows[k % 25].elements[0].durationInTraffic.inSeconds;
					localities.get(k).distance = ((float) dist) / 1000f;
					localities.get(k).speed[day - 1][hour] = getSpeedInkmph(dist, time);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private static Date getDate(int day, int hour) {
		Calendar c = Calendar.getInstance();
		if (day == 1) {
			c.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);
		} else if (day == 2) {
			c.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
		} else if (day == 3) {
			c.set(Calendar.DAY_OF_WEEK,Calendar.TUESDAY);
		} else if (day == 4) {
			c.set(Calendar.DAY_OF_WEEK,Calendar.WEDNESDAY);
		} else if (day == 5) {
			c.set(Calendar.DAY_OF_WEEK,Calendar.THURSDAY);
		} else if (day == 6) {
			c.set(Calendar.DAY_OF_WEEK,Calendar.FRIDAY);
		} else if (day == 7) {
			c.set(Calendar.DAY_OF_WEEK,Calendar.SATURDAY);
		}
		c.add(Calendar.DATE,7);
		c.set(Calendar.HOUR_OF_DAY, hour);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		return c.getTime();
	}
	
	private static float getSpeedInkmph(long dist, long time) {
		float speed = dist;
		speed /= 1000.0f; // kms conversion
		speed /= time; // speed in kmpsec
		speed *= 3600; // speed in kmph
		return speed;
	}
	
	public static void createTimeProfileFile() throws IOException {
		String empFilePath = ConfigVariables.currentFolderPath + '/' + timeProfileFileName;
		fileWriter = new FileWriter(empFilePath, false);
		fileWriter.write("BU,Anchor Geocode,Time of Day,Direction,Day,Time Profile\n");
		for (int day = 1; day <= 7; ++day) {
			for (int hours = 0; hours < 24; ++hours) {
				fileWriter.write(ConfigVariables.BUID);
				fileWriter.write(',');
				fileWriter.write("\"" + ConfigVariables.anchorGeocode + "\"");
				fileWriter.write(',');
				fileWriter.write(getTimeOfDay(hours));
				fileWriter.write(',');
				fileWriter.write("IN");
				fileWriter.write(',');
				fileWriter.write(String.valueOf(day));
				fileWriter.write(',');
				fileWriter.write(getTimeProfile(day, hours));
				fileWriter.write('\n');
			}
		}
		fileWriter.flush();
		fileWriter.close();
	}
	
	public static void createPickUpTimeChart(List<Locality> localities) throws IOException {
		String empFilePath = ConfigVariables.currentFolderPath + '/' + ConfigVariables.anchorGeocode + pickupFileName;
		fileWriter = new FileWriter(empFilePath, false);
		fileWriter.write("localityid,Nodal Point,Locality,Distance");
		for (int day = 1; day <= 7; ++day) {
			for (int hours = 0; hours < 24; ++hours) {
				fileWriter.write(',');
				fileWriter.write(getTimeProfile(day, hours));
			}
		}
		fileWriter.write('\n');
		for (Locality locality : localities) {
			fileWriter.write(locality.localityId);
			fileWriter.write(',');
			fileWriter.write(locality.nodalPoint);
			fileWriter.write(',');
			fileWriter.write(locality.locality);
			fileWriter.write(',');
			fileWriter.write(String.format("%.02f", locality.distance));
			for (int day = 0; day < 7; ++day) {
				for (int hours = 0; hours < 24; ++hours) {
					fileWriter.write(',');
					fileWriter.write(String.format("%.02f", locality.speed[day][hours]));
				}
			}
			fileWriter.write('\n');
		}
		fileWriter.flush();
		fileWriter.close();
	}
}

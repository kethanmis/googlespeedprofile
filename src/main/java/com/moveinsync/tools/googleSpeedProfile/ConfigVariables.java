package com.moveinsync.tools.googleSpeedProfile;

import java.util.Properties;

public class ConfigVariables {

	public static String BUID = "";
	public static String direction = "";
	public static String anchorGeocode = "";
	public static String localityfile = "";
	public static String currentFolderPath = "";
	
	public static void initConfigs(Properties properties) {
		BUID = properties.getProperty("BUID");
		direction = properties.getProperty("Direction");
		anchorGeocode = properties.getProperty("Anchor_Geocode");
		currentFolderPath = System.getProperty("user.dir");
		localityfile = currentFolderPath + '/' + properties.getProperty("LocalityFile");
	}
}

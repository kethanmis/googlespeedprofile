package com.moveinsync.tools.googleSpeedProfile;

public class Locality {
	String localityId;
	String nodalPoint;
	String locality;
	String geocode;
	float distance;
	// 7*24 speed values
	float[][] speed;
	
	public Locality() {
		speed = new float[7][24];
		for (int i = 0; i < 7; ++i) {
			for (int j = 0; j < 24; ++j) {
				speed[i][j] = -1;
			}
		}
		distance = 0.0f;
	}
	
	public Locality(String localityId, String nodalPoint, String locality, String geocode) {
		this();
		this.localityId = localityId;
		this.nodalPoint = nodalPoint;
		this.locality = locality;
		this.geocode = geocode;
	}
}
